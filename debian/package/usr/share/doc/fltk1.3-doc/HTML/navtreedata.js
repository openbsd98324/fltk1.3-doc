/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "FLTK 1.3.5", "index.html", [
    [ "FLTK Programming Manual", "index.html", "index" ],
    [ "Todo List", "todo.html", null ],
    [ "Deprecated List", "deprecated.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"Enumerations_8H.html",
"Enumerations_8H.html#ad16daf120d9a0501cccaee563af0b9a3a9edbfd236c9ea348dcaae935a76bd885",
"Fl__Printer_8H.html",
"classFl__Browser__.html#a43dd6231f2684f132a5a33b5d7d0c8e7",
"classFl__FLTK__File__Chooser.html#ae27a6b7c7074ef5ca3245f8059cc8a52",
"classFl__Graphics__Driver.html#a51cbf82193734db68117b530cc2220da",
"classFl__Input__.html#a1eb1ab33b4f6fb29858490136df1e7f5",
"classFl__Paged__Device.html#aa4712662227b4cec437669b5bac821fe",
"classFl__Preferences_1_1Node.html#a2e1c72ec0e94f9d83ec4a8d3a57b6b01",
"classFl__Sys__Menu__Bar.html#ad9d9b1af865fb2acbac2dd687f23a36b",
"classFl__Text__Buffer.html#aa2c392f5efd9f75762ced592a75b9860",
"classFl__Tiled__Image.html#a94b5af1f3b1a8ee1cf12929c5821d986",
"classFl__Tree__Item.html#a9d045540a28f635706eb8dfc8d604832",
"classFl__Widget.html#a7dcd6fc8c6108fb68a00bf922cc79022",
"editor.html#editor_replace_cb",
"forms.html#forms_problems",
"group__fl__attributes.html#ga54d75920c0484916cd6b2101937ec5ff",
"group__fl__drawings.html#gaf715c7239d76c49737eb514e440490b5",
"group__group__comdlg.html#gac761d3edfeb4b4e78d36c4c0d19c8906",
"structFl__Menu__Item.html#a9839d5dd7ecc444d4593daeca3c3d0a7"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';